Chrome OS eksik veya hasarlı.
Lütfen bir kurtarma USB çubuğu veya SD kartı takın.
Lütfen bir kurtarma USB çubuğu takın.
Lütfen bir kurtarma USB çubuğu veya SD kartı takın (not: kurtarma işleminde mavi USB bağlantı noktası KULLANILMAZ).
Lütfen cihazın ARKASINDAKİ 4 bağlantı noktasından birine bir USB çubuğu takın.
Taktığınız cihazda Chrome OS yok.
OS doğrulama KAPALI
Yeniden etkinleştirmek için BOŞLUK tuşuna basın
OS doğrulamayı açmak istediğinizi onaylamak için ENTER tuşuna basın.
Sisteminiz yeniden başlatılacak ve yerel veriler temizlenecek.
Geri gitmek için ESC tuşuna basın.
OS doğrulama AÇIK.
OS doğrulamayı KAPATMAK için ENTER tuşuna basın.
Yardım için https://google.com/chromeos/recovery adresini ziyaret edin
Hata kodu
Kurtarma işlemini başlatmak için tüm harici cihazları çıkarın.
Model 60061e
OS doğrulamayı KAPATMAK için KURTARMA düğmesine basın.
Bağlı güç kaynağı, bu cihazı çalıştırmaya yetecek güce sahip değil.
Chrome OS şimdi kapatılacak.
Lütfen doğru adaptörü kullanın ve tekrar deneyin.
Lütfen tüm bağlı cihazları çıkarın ve kurtarma işlemini başlatın.
Alternatif bootloader için lütfen bir rakam tuşuna basın:
Teşhis aracını çalıştırmak için GÜÇ düğmesine basın.
OS doğrulamayı KAPATMAK için GÜÇ düğmesine basın.
