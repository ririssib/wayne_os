Mga Opsyon ng Developer
Ipakita ang Impormasyon ng Pag-debug
I-enable ang Pag-verify ng OS
I-off
Wika
I-boot mula sa Network
I-boot ang Legacy BIOS
I-boot mula sa USB
Mag-boot Mula sa USB o SD Card
I-boot mula sa Internal na Disk
Kanselahin
Kumpirmahin ang Pag-enable sa Pag-verify ng OS
I-disable ang Pag-verify ng OS
Kumpirmahin ang Pag-disable sa Pag-verify ng OS
Gamitin ang mga volume button upang mag-navigate pataas o pababa
at ang power button upang pumili ng opsyon.
Kung idi-disable ang Pag-verify ng OS, magiging HINDI LIGTAS ang iyong system.
Piliin ang "Kanselahin" upang patuloy na maprotektahan.
NAKA-OFF ang pag-verify ng OS. HINDI LIGTAS ang iyong system.
Piliin ang "I-enable ang Pag-verify ng OS" upang maging ligtas muli.
Piliin ang "Kumpirmahin ang Pag-enable sa Pag-verify ng OS" upang maprotektahan ang iyong system.
