# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

DROP TABLE IF EXISTS `tko_jobs`;
CREATE TABLE `tko_jobs` (
  `job_idx` int(10) unsigned NOT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `machine_idx` int(10) unsigned NOT NULL,
  `queued_time` datetime DEFAULT NULL,
  `started_time` datetime DEFAULT NULL,
  `finished_time` datetime DEFAULT NULL,
  `afe_job_id` int(11) DEFAULT NULL,
  `afe_parent_job_id` int(11) default NULL,
  `build` varchar(255) default NULL,
  `build_version` varchar(255) default NULL,
  `suite` varchar(40) default NULL,
  `board` varchar(40) default NULL,
  PRIMARY KEY (`job_idx`)
);


DROP TABLE IF EXISTS `tko_job_keyvals`;
CREATE TABLE `tko_job_keyvals` (
  `id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `key` varchar(90) NOT NULL,
  `value` varchar(300) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `tko_job_keyvals_job_id` (`job_id`),
  KEY `tko_job_keyvals_key` (`key`)
);


DROP TABLE IF EXISTS `tko_tests`;
CREATE TABLE `tko_tests` (
  `test_idx` int(10) unsigned NOT NULL,
  `job_idx` int(10) unsigned NOT NULL,
  `test` varchar(300) DEFAULT NULL,
  `subdir` varchar(300) DEFAULT NULL,
  `kernel_idx` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `reason` varchar(4096) DEFAULT NULL,
  `machine_idx` int(10) unsigned NOT NULL,
  `invalid` tinyint(1) DEFAULT '0',
  `finished_time` datetime DEFAULT NULL,
  `started_time` datetime DEFAULT NULL,
  `invalidates_test_idx` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`test_idx`),
  KEY job_idx (job_idx),
  KEY `started_time` (`started_time`)
);


DROP TABLE IF EXISTS `tko_status`;
CREATE TABLE `tko_status` (
  `status_idx` int(10) unsigned NOT NULL,
  `word` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`status_idx`)
);


DROP TABLE IF EXISTS `tko_machines`;
CREATE TABLE `tko_machines` (
  `machine_idx` int(10) unsigned NOT NULL,
  `hostname` varchar(700) DEFAULT NULL,
  `machine_group` varchar(80) DEFAULT NULL,
  `owner` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`machine_idx`)
);


DROP TABLE IF EXISTS `import_tko_jobs`;
CREATE TABLE `import_tko_jobs` (
  `job_idx` int(10) unsigned NOT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `username` varchar(80) DEFAULT NULL,
  `machine_idx` int(10) unsigned NOT NULL,
  `queued_time` datetime DEFAULT NULL,
  `started_time` datetime DEFAULT NULL,
  `finished_time` datetime DEFAULT NULL,
  `afe_job_id` int(11) DEFAULT NULL,
  `afe_parent_job_id` int(11) default NULL,
  `build` varchar(255) default NULL,
  `build_version` varchar(255) default NULL,
  `suite` varchar(40) default NULL,
  `board` varchar(40) default NULL,
  PRIMARY KEY (`job_idx`)
);

DROP TABLE IF EXISTS `import_tko_job_keyvals`;
CREATE TABLE `import_tko_job_keyvals` (
  `id` int(11) NOT NULL,
  `job_id` int(10) unsigned NOT NULL,
  `key` varchar(90) NOT NULL,
  `value` varchar(300) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `tko_job_keyvals_job_id` (`job_id`),
  KEY `tko_job_keyvals_key` (`key`)
);

DROP TABLE IF EXISTS `import_tko_tests`;
CREATE TABLE `import_tko_tests` (
  `test_idx` int(10) unsigned NOT NULL,
  `job_idx` int(10) unsigned NOT NULL,
  `test` varchar(300) DEFAULT NULL,
  `subdir` varchar(300) DEFAULT NULL,
  `kernel_idx` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `reason` varchar(4096) DEFAULT NULL,
  `machine_idx` int(10) unsigned NOT NULL,
  `invalid` tinyint(1) DEFAULT '0',
  `finished_time` datetime DEFAULT NULL,
  `started_time` datetime DEFAULT NULL,
  `invalidates_test_idx` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`test_idx`),
  KEY `started_time` (`started_time`)
);


DROP TABLE IF EXISTS `import_tko_status`;
CREATE TABLE `import_tko_status` (
  `status_idx` int(10) unsigned NOT NULL,
  `word` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`status_idx`)
);


DROP TABLE IF EXISTS `import_tko_machines`;
CREATE TABLE `import_tko_machines` (
  `machine_idx` int(10) unsigned NOT NULL,
  `hostname` varchar(700) DEFAULT NULL,
  `machine_group` varchar(80) DEFAULT NULL,
  `owner` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`machine_idx`)
);

# build info harvested from the buildbots
DROP TABLE IF EXISTS `builds`;
CREATE TABLE `builds` (
  `build_idx` bigint DEFAULT NULL,
  `buildbot_root` varchar(511) DEFAULT NULL,
  `builder_name` varchar(100) DEFAULT NULL,
  `buildname` varchar(100) DEFAULT NULL,
  `build` varchar(100) DEFAULT NULL,
  `release_number` int DEFAULT NULL,
  `config` varchar(45) DEFAULT NULL,
  `board` varchar(45) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `completed` tinyint DEFAULT NULL,
  `result` int DEFAULT NULL,
  `simplified_result` tinyint DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `chromever` varchar(100) DEFAULT NULL,
  `reason` varchar(2000) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `build_major` int DEFAULT NULL,
  `build_minor` int DEFAULT NULL,
  `build_maintenance` int DEFAULT NULL,
  `build_remaining` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`release_number`, `platform`,`build`, `number`)
);

DROP TABLE IF EXISTS `autobugs`;
CREATE TABLE `autobugs` (
  `jkv_id` int unsigned NOT NULL,
  `test_idx` int unsigned NOT NULL,
  `bug_id` int unsigned NOT NULL,
  PRIMARY KEY (`test_idx`, `bug_id`)
);

DROP TABLE IF EXISTS import_autobugs;
CREATE TABLE import_autobugs like autobugs;
