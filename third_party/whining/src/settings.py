# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Config for a dashboard instance. DB usernames etc."""

import ConfigParser
import distutils.util
import os
import sys


DEFAULT_CONFIG_INI = os.path.join('.', 'config.ini')
AUTOTEST_DB_NAME = 'autotest'
RAW_DB_NAME = 'raw'
WMATRIX_DB_NAME = 'wmatrix'
BACKUP_DB_NAME = 'backup'

# Default values if unset in config.ini file
DEFAULT_AUTOTEST_HOST = ''
DEFAULT_AUTOTEST_USER = 'chromeos-test'
DEFAULT_DAYS_BACK = 3
DEFAULT_FILTER_AUTOTEST_DB = 'False'
DEFAULT_FILTER_B_BUILDS = 'False'
DEFAULT_RELATIVE_ROOT = ''
DEFAULT_SUITE_SCHEDULER_INI = '/usr/local/autotest/suite_scheduler.ini'
DEFAULT_STAINLESS_URL = ''

class Settings(object):
    """Global settings for WMatrix instance."""


    def __init__(self, config_ini=DEFAULT_CONFIG_INI):
        """Open config_ini configuration file, and read settings from it."""
        self._config = ConfigParser.SafeConfigParser()
        self._config.read(config_ini)

        # TODO (drinkcat): Put default values in a constant dict
        general = dict(self._config.items('general'))
        self.autotest_host = general.get('autotest_host',
                                         DEFAULT_AUTOTEST_HOST)
        self.autotest_user = general.get('autotest_user',
                                         DEFAULT_AUTOTEST_USER)
        self.default_days_back = int(general.get('default_days_back',
                                                 DEFAULT_DAYS_BACK))
        self.filter_autotest_db = distutils.util.strtobool(
                                   general.get('filter_autotest_db',
                                               DEFAULT_FILTER_AUTOTEST_DB))
        self.filter_b_builds = distutils.util.strtobool(
                                   general.get('filter_b_builds',
                                               DEFAULT_FILTER_B_BUILDS))
        self.relative_root = general.get('relative_root', DEFAULT_RELATIVE_ROOT)
        self.suite_scheduler_ini = general.get('suite_scheduler_ini',
                                               DEFAULT_SUITE_SCHEDULER_INI)
        self.stainless_url = general.get('stainless_url', DEFAULT_STAINLESS_URL)

    def get_db(self, name):
        """Create a database settings object for a given database name.

        @param name: Database name (autotest, raw, wmatrix). The name of the
                     configuration section is name + "_db".

        """
        fullname = name + '_db'
        p = dict(self._config.items(fullname))
        _type = p.pop('type')
        if _type == 'mysql':
            return DatabaseMySQL(p)
        elif _type == 'speckle':
            return DatabaseSpeckle(p)
        elif _type == 'gae':
            return DatabaseGAE(p)


class Database(object):
    """Superclass for database settings (MySQL, Speckle, Cloud)."""
    pass


class DatabaseMySQL(Database):
    """Settings for local MySQL DB"""


    def __init__(self, db_params):
        self.db_params = db_params
        # Import MySQLdb module
        try:
            import MySQLdb
            self.db_module = MySQLdb
        except ImportError:
            sys.exit("Couldn't import MySQLdb module for connecting to "
                     "MySQL instance. Try apt-get install python-mysqldb")


class DatabaseSpeckle(Database):
    """Settings for using CloudSQL from a locally running dashboard.

    On first connection attempt GAE SDK lib will open a browser window and ask
    you to authorize this connection. For more details:
    https://developers.google.com/cloud-sql/docs/external
    """


    def __init__(self, db_params):
        self.db_params = db_params
        try:
            if os.environ.get('GAEDIR'):
                # When the default python library already have package 'google'
                # e.g. /usr/lib/python2.7/dist-packages/google
                # the default google package will be used, so that it won't
                # see the google package defined in PYTHONPATH
                # We have to put the appengine_path before the default search
                # path
                sys.path.insert(0, os.environ['GAEDIR'])
            from google.storage.speckle.python.api import rdbms_googleapi
            self.db_module = rdbms_googleapi
        except ImportError:
            msg = ("Couldn't import rdbms_googleapi for connecting to "
                   "CloudSQL instance. "
                   "You may need to download AppEngine SDK and set up "
                   "pythonpath like this: "
                   "export PYTHONPATH=$PYTHONPATH:$HOME/google_appengine;\n"
                   "If you already have any google package installed in "
                   "your system path, you'll need to set up like this: "
                   "export GAEDIR=$HOME/google_appengine"
                   )
            sys.exit(msg)


class DatabaseGAE(Database):
    """Settings for CloudSQL access from within App Engine.

    For Cloud SQL there are 2 layers of authentication:
    1. You have to authorize you application via GAE administration console to
       access the Cloud SQL instance.
    2. MySQL style permission scheme within the instance. By default the
       instance has a root user with empty password which is used if no username
       is specified on connection.
    For more details:
    developers.google.com/appengine/docs/python/cloud-sql/developers-guide
    """


    def __init__(self, db_params):
        self.db_params = db_params
        from google.appengine.api import rdbms
        self.db_module = rdbms


settings = None
