# Copyright (c) 2009 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

app-accessibility/brltty	-beeper -contracted-braille -doc -fm -gpm -java -learn-mode -midi -ncurses -ocaml -pcm -speech -tcl -X
app-admin/logrotate	-cron
app-admin/rsyslog	-gcrypt -ssl -zlib
app-benchmarks/i7z	-X
app-i18n/zinnia		-perl
app-editors/nano	-unicode
app-editors/qemacs	-X
app-editors/vim		-X minimal
# We don't use network functionality in bash, and since we ship it in a release
# image, we'd prefer that attack surface simply not exist.
app-shells/bash		-net
# Drop this once we audit code to make sure we aren't using `echo -n` or `echo -e`.
app-shells/dash	vanilla
app-text/poppler	cairo
dev-cpp/glog		gflags
dev-db/sqlite		-extensions
# Disable Segger J-Link for building openocd-0.10.0. We currently
# don't use J-Link. It requires upgrading libjaylink, which is not yet
# supported by official Gentoo portage repository.
dev-embedded/openocd	-jlink
dev-lang/perl		-berkdb gdbm
dev-lang/python		-berkdb gdbm -sqlite
dev-libs/dbus-glib	tools
dev-libs/elfutils	-utils
dev-libs/expat		-unicode
dev-libs/glib		-doc
dev-libs/libp11         -bindist
# Readline is used by pcretest which we don't use.
# zlib/bzip2 are only used by pcregrep which we don't use.
dev-libs/libpcre	-bzip2 -readline -zlib
dev-libs/leveldb	-tcmalloc
dev-libs/nss		-utils
dev-libs/opencryptoki	tpmtok
dev-libs/opensc	-pcsc-lite ctapi
dev-libs/openssl	pkcs11 -tls-heartbeat
dev-python/pygobject	cairo
dev-python/pyudev	pygobject
dev-util/dialog		-unicode minimal
dev-util/perf		-audit -doc -demangle -tui -ncurses -perl -python -slang -unwind
chromeos-base/chromeos-chrome   build_tests
>=chromeos-base/chromeos-chrome-42.0.2297.0_rc-r1		v4l2_codec
chromeos-base/nsswitch	zeroconf
media-gfx/imagemagick	jpeg png svg tiff
# Disable all SANE backends that don't support USB.
media-gfx/sane-backends -sane_backends_abaton -sane_backends_agfafocus -sane_backends_apple -sane_backends_artec -sane_backends_bh -sane_backends_canon -sane_backends_coolscan -sane_backends_dc210 -sane_backends_dc240 -sane_backends_dc25 -sane_backends_dmc -sane_backends_hs2p -sane_backends_ibm -sane_backends_kodak -sane_backends_leo -sane_backends_matsushita -sane_backends_microtek -sane_backends_mustek -sane_backends_nec -sane_backends_net -sane_backends_p5 -sane_backends_pie -sane_backends_plustek_pp -sane_backends_qcam -sane_backends_ricoh -sane_backends_s9036 -sane_backends_sceptre -sane_backends_sharp -sane_backends_sp15c -sane_backends_st400 -sane_backends_tamarack -sane_backends_teco1 -sane_backends_teco2 -sane_backends_teco3 -sane_backends_test -sane_backends_umax_pp
media-gfx/zbar		-gtk -imagemagick jpeg python -qt4 threads -v4l -X -xv
media-libs/freeimage	png
# TODO(derat): Unset glib for harfbuzz if/when pango is no longer needed:
# http://crbug.com/691477
media-libs/harfbuzz	-cairo -introspection
media-libs/libdvdread	-css
media-libs/libsndfile	minimal
# disabled in profiles/default/linux/package.use
media-libs/mesa		gallium classic shared-glapi gles2 egl -gbm
media-libs/opencv	-gtk python png jpeg -tiff v4l
media-libs/piglit	waffle
media-libs/waffle	gbm
media-sound/sox		alsa ogg
media-video/mplayer	cpudetection fbcon -encode -ass -a52 -cdio -dirac -dts -dv -dvd -dvdnav -enca -faac -faad -live -quicktime -mp3 -rar -real -speex -schroedinger -theora -tremor -toolame -twolame -vorbis -xscreensaver -x264 -xv -xvid
net-analyzer/tcpdump	-chroot
net-dns/avahi-daemon	zeroconf
net-dns/dnsmasq		script
net-firewall/iptables	conntrack pcap
net-fs/samba		ads acl gnutls ldap winbind -addc -addns -ceph -cluster -gpg -perl -python -test
# C++ library of gnutls uses exceptions, which breaks with -fno-exceptions flag.
# Clients don't need the C++ library anyway, so disabling it...
net-libs/gnutls	-cxx
# We want HTTPS support in web server based on libmicrohttpd.
# Also |messages| is for enabling POST processor in the library.
net-libs/libmicrohttpd	ssl messages
net-libs/libsoup	-ssl
net-misc/curl		ares
net-misc/dhcpcd		crash
net-misc/iperf		threads
# arping is used by some wifi autotests.
# tracepath is exposed to the user via crosh.
net-misc/iputils	arping tracepath
net-misc/ntp		caps
net-misc/openssh	-hpn -X
net-vpn/openvpn		pkcs11
net-vpn/strongswan	cisco nat-transport -strongswan_plugins_unity pkcs11
net-nds/openldap	minimal -cxx
net-print/cups		-dbus zeroconf upstart
net-print/cups-filters	-dbus -pclm -pdf -zeroconf
net-print/hplip		minimal
# squid requires gnutls 3.1.5 for ssl, default it to off until newer gnutls is available
net-proxy/squid		-ssl
net-proxy/tsocks	tordns
# No support for bluetooth printing.
net-wireless/bluez	-cups
net-wireless/wpa_supplicant	dbus debug -readline smartcard ssl
net-wireless/wpa_supplicant-2_8	dbus debug -readline smartcard ssl
sci-geosciences/gpsd	-python -ntp -X dbus garmin minimal ocean tntc usb -sockets
# Build only TF lite to save disk space.
sci-libs/tensorflow	-python minimal
sys-apps/busybox	-pam -selinux
sys-apps/baselayout	-auto_seed_etc_files
sys-apps/coreutils	multicall
sys-apps/dbus		-X
sys-apps/fwupd		-gpg -man
sys-apps/hwids		-net
sys-apps/mawk		forced-sandbox
sys-apps/sed		forced-sandbox
sys-apps/smartmontools	minimal -daemon
sys-apps/util-linux     -unicode -udev
# pam_unix: empty password is not OK
sys-auth/pambase	-nullok
sys-block/fio		aio
sys-block/parted	device-mapper
sys-devel/bc		forced-sandbox
sys-devel/clang		-multitarget
sys-devel/llvm		-multitarget -ncurses
sys-fs/avfs		-extfs
sys-fs/lvm2		-lvm1 -readline -static device-mapper-only -thin
sys-fs/mtools		-X
sys-fs/ntfs3g		-crypt external-fuse ntfsprogs -suid
sys-fs/udev		-devfs-compat -rule_generator hwdb acl
sys-kernel/chromeos-kernel-3_8 -clang
sys-kernel/chromeos-kernel-3_10 -clang
sys-kernel/chromeos-kernel-3_14 -clang
sys-kernel/chromeos-kernel-3_18 -clang
sys-kernel/linux-firmware	linux_firmware_keyspan_usb
sys-libs/ldb		-python
sys-libs/ncurses	-cxx -unicode minimal tinfo
sys-libs/pam		-berkdb
sys-libs/talloc		-python
sys-libs/tdb		-python
sys-libs/tevent		-python
sys-libs/zlib		static-libs
sys-power/powertop	-unicode
sys-process/htop	-unicode
sys-process/procps	-unicode
x11-apps/intel-gpu-tools	-python
x11-apps/xinit		minimal
x11-base/xorg-server	-suid
x11-libs/libdrm		libkms
x11-libs/libdrm-tests	libkms
x11-libs/cairo		-opengl
x11-libs/libva		egl
# Need png for color font support. bzip2 is not used. It's only for X11 *pcf.bz2
# that we don't have.
media-libs/freetype	png -bzip2
