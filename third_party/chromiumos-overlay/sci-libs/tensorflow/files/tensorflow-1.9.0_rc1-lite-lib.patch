From dbfaf5786e58f546b901311be1850a283fdb9b6b Mon Sep 17 00:00:00 2001
From: Michael Martis <martis@chromium.org>
Date: Fri, 6 Jul 2018 11:29:47 +1000
Subject: [PATCH] Added a TF lite shared library target.

Typically TF lite projects are built with Bazel, so there is no TF
target to produce a standalone TF lite shared library. Since we want to
use TF lite in projects with arbitrary build systems, we add such a
target.

We also (coarsely) filter symbols exported by the library to minimize
its size.

The TF team intends to eventually support a target like this: see
https://github.com/tensorflow/tensorflow/issues/20905

Author: martis@chromium.org
---
 tensorflow/contrib/lite/BUILD              | 23 +++++++++++++++++++++-
 tensorflow/contrib/lite/version_script.lds |  9 +++++++++
 2 files changed, 31 insertions(+), 1 deletion(-)
 create mode 100644 tensorflow/contrib/lite/version_script.lds

--- a/tensorflow/contrib/lite/BUILD
+++ b/tensorflow/contrib/lite/BUILD
@@ -4,7 +4,8 @@ package(default_visibility = [
 
 licenses(["notice"])  # Apache 2.0
 
-load("//tensorflow/contrib/lite:build_def.bzl", "tflite_copts", "gen_selected_ops")
+load("//tensorflow/contrib/lite:build_def.bzl", "tflite_copts", "tflite_linkopts", "gen_selected_ops")
+load("//tensorflow:tensorflow.bzl", "tf_cc_shared_object")
 
 exports_files(glob([
     "testdata/*.bin",
@@ -92,6 +93,26 @@ cc_library(
 
 exports_files(["builtin_ops.h"])
 
+LINKER_SCRIPT = "version_script.lds"
+
+tf_cc_shared_object(
+    name = "libtensorflow_lite.so",
+    framework_so = [],
+    copts = ["-DFARMHASH_NO_CXX_STRING"],
+    linkopts = tflite_linkopts() + [
+        "-s",                      # Strip library.
+        "-Wl,--version-script",    # Export only relevant symbols.
+        "$(location {})".format(LINKER_SCRIPT)
+    ],
+    linkstatic = 1,
+    visibility = ["//visibility:public"],
+    deps = [
+        ":framework",
+        "//tensorflow/contrib/lite/kernels:builtin_ops",
+        LINKER_SCRIPT,
+    ]
+)
+
 cc_library(
     name = "string",
     hdrs = [
--- /dev/null
+++ b/tensorflow/contrib/lite/version_script.lds
@@ -0,0 +1,9 @@
+VERS_1.0 {
+  # Export all TF-lite symbols.
+  global:
+    *tflite*;
+
+  # Hide everything else.
+  local:
+    *;
+};
-- 
2.18.0.345.g5c9ce644c3-goog

