# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import("//common-mk/pkg_config.gni")

group("all") {
  deps = [
    ":arc-setup",
    ":generate_squashfs",
  ]
  if (use.test) {
    deps += [ ":arc-setup_testrunner" ]
  }
  if (use.fuzzer) {
    deps += [
      ":arc_setup_util_expand_property_contents_fuzzer",
      ":arc_setup_util_find_all_properties_fuzzer",
      ":arc_setup_util_find_fingerprint_and_sdk_version_fuzzer",
    ]
  }
}

pkg_config("target_defaults_pkg_deps") {
  pkg_deps = [
    "libbrillo-${libbase_ver}",
    "libchrome-${libbase_ver}",
    "libcros_config",
    "libcrypto",
    "libcryptohome-client",
    "libmetrics-${libbase_ver}",
    "libminijail",
    "libselinux",

    # system_api depends on protobuf (or protobuf-lite). It must appear
    # before protobuf here or the linker flags won"t be in the right
    # order.
    "system_api",
    "protobuf-lite",
  ]
}

config("target_defaults") {
  configs = [ ":target_defaults_pkg_deps" ]
  defines = [ "OS_CHROMEOS" ]
}

static_library("libarc_setup") {
  configs += [ ":target_defaults" ]
  sources = [
    "arc_read_ahead.cc",
    "arc_setup.cc",
    "arc_setup_metrics.cc",
    "arc_setup_util.cc",
    "art_container.cc",
    "config.cc",
  ]
  defines = []
  if (defined(use.houdini) && use.houdini) {
    defines += [ "USE_HOUDINI=1" ]
  }
  if (defined(use.houdini64) && use.houdini64) {
    defines += [ "USE_HOUDINI64=1" ]
  }
  if (defined(use.ndk_translation) && use.ndk_translation) {
    defines += [ "USE_NDK_TRANSLATION=1" ]
  }
  libs = [ "bootstat" ]
}

executable("arc-setup") {
  configs += [ ":target_defaults" ]
  sources = [
    "main.cc",
  ]
  deps = [
    ":libarc_setup",
  ]
}

action("mkdir_squashfs_source_dir") {
  inputs = []
  outputs = [
    "${root_gen_dir}/squashfs_source_dir",
  ]
  script = "//common-mk/file_generator_wrapper.py"
  args = [
    "mkdir",
    "-p",
    "${root_gen_dir}/squashfs_source_dir",
  ]
}

action("generate_squashfs") {
  deps = [
    ":mkdir_squashfs_source_dir",
  ]
  inputs = [
    "${root_gen_dir}/squashfs_source_dir",
  ]
  outputs = [
    "${root_out_dir}/dev-rootfs.squashfs",
  ]
  script = "//common-mk/file_generator_wrapper.py"
  args = [
    "mksquashfs",
    "${root_gen_dir}/squashfs_source_dir",
    "${root_out_dir}/dev-rootfs.squashfs",
    "-no-progress",
    "-info",
    "-all-root",
    "-noappend",
    "-comp",
    "lzo",
    "-b",
    "4K",

    # Create rootfs and necessary dev nodes for art container.
    # ashmem minor number is dynamic determined and will be bind
    # mounted.
    "-p",
    "/dev d 700 0 0",
    "-p",
    "/dev/ashmem c 666 root root 1 3",
    "-p",
    "/dev/random c 666 root root 1 8",
    "-p",
    "/dev/urandom c 666 root root 1 9",
  ]
}

if (use.test) {
  pkg_config("arc-setup_testrunner_pkg_deps") {
    pkg_deps = [
      "libbrillo-test-${libbase_ver}",
      "libchrome-test-${libbase_ver}",
    ]
  }
  executable("arc-setup_testrunner") {
    configs += [
      "//common-mk:test",
      ":arc-setup_testrunner_pkg_deps",
      ":target_defaults",
    ]
    deps = [
      ":libarc_setup",
      "../../common-mk/testrunner:testrunner",
    ]

    # TODO(xzhou): Move boot_lockbox_client.cc and
    # priv_code_verifier.cc back to libarc_setup.
    sources = [
      "arc_read_ahead_test.cc",
      "arc_setup_metrics_test.cc",
      "arc_setup_test.cc",
      "arc_setup_util_test.cc",
      "art_container_test.cc",
      "boot_lockbox_client.cc",
      "config_test.cc",
      "priv_code_verifier.cc",
    ]
  }
}

if (use.fuzzer) {
  executable("arc_setup_util_find_all_properties_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":target_defaults",
    ]
    sources = [
      "arc_setup_util_find_all_properties_fuzzer.cc",
    ]
    deps = [
      ":libarc_setup",
    ]
  }

  executable("arc_setup_util_find_fingerprint_and_sdk_version_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":target_defaults",
    ]
    sources = [
      "arc_setup_util_find_fingerprint_and_sdk_version_fuzzer.cc",
    ]
    deps = [
      ":libarc_setup",
    ]
  }

  executable("arc_setup_util_expand_property_contents_fuzzer") {
    configs += [
      "//common-mk/common_fuzzer",
      ":target_defaults",
    ]
    sources = [
      "arc_setup_util_expand_property_contents_fuzzer.cc",
    ]
    deps = [
      ":libarc_setup",
    ]
  }
}
